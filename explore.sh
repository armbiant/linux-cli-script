readonly CXXFLAGS="-std=c++23 -O1"

g++ main.cxx $CXXFLAGS -S -o - \
	| c++filt \
	| grep -vE "\s+\.|^[0-9]:"

