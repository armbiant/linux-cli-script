# Command line Compiler Explorer

A Linux command line compiler explorer script inspired by [this
article](https://queue.acm.org/detail.cfm?id=3372264) by Matt Godbolt. See the
[repo](https://gitlab.com/germs-dev/explore) that generated this page.

## Installation
```bash
sudo apt update
sudo apt install g++ curl entr gedit

git clone git@gitlab.com:germs-dev/explore
cd explore/
```

## Example usage
Below I'm splitting my terminal with `tmux` and running the main script when
the source changes using `entr`.

```bash
ls main.cxx | entr -c bash explore.sh
```

![](term.jpg)

But you can also use this wrapper script to edit in `gedit`.

```bash
bash run.sh
```

## The bash script `explore.sh`
This command really is the core of [godbolt.org](https://godbolt.org/), edit
your compiler flags here.

