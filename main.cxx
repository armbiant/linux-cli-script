#include <cmath>
#include <cstddef>

int func(const size_t max) {

  int sum = 0;

  for (auto i = 0uz; i < max; ++i)
    sum += i;

  return sum;
}

int main() {
  const auto param = std::pow(2, 13);
  return func(param * 3);
}
